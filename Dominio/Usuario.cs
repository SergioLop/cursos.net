using Microsoft.AspNetCore.Identity;

namespace Dominio
{   //sirve para asignar un key para lamigracion
    public class Usuario : IdentityUser
    {
        public string NombreCompleto {get;set;}
    }
}