using System;
namespace Dominio




{    //objetos negocio componentes de todas las clases
    //operador ? permite que sea pueda obtener un establecer un valor null
    public class Comentario
    {
        public Guid ComentarioId{get;set;}
        public string Alumno{get;set;}
        public int Puntaje{get;set;}
        public string ComentarioTexto {get;set;}
        public Guid CursoId {get;set;}

        public DateTime? FechaCreacion {get;set;}
        public Curso Curso {get;set;}
    }
}