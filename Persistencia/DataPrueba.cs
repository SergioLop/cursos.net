using System.Linq;
using System.Threading.Tasks;
using Dominio;
using Microsoft.AspNetCore.Identity;

namespace Persistencia
{ ////creacion de migracion personalizada 
    public class DataPrueba
    {
        public static async Task InsertarData(CursosOnlineContext context, UserManager<Usuario> usuarioManager){
            if(!usuarioManager.Users.Any()){
                var usuario = new Usuario{NombreCompleto = "sergio", UserName="sergio", Email="1@gmail.com"};
                await usuarioManager.CreateAsync(usuario, "Password123$");
            }
        }
    }
}